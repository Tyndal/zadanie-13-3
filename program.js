process.stdin.setEncoding("utf-8");
process.stdin.on("readable", function() {
  const input = process.stdin.read();
  if (input !== null) {
    const instruction = input.toString().trim();
    switch (instruction) {
      case "/exit":
        process.stdout.write("Quittin app!\n");
        process.exit();
        break;
      case "/version":
        process.stdout.write(`Node version: ${process.versions.node}\n`);
        break;
      default:
        process.stderr.write("Wrong instructions!\n");
    }
  }
});
